import 'package:flutter/material.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: ConstrainedBox(
        constraints: const BoxConstraints(maxWidth: 300.0),
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Spacer(flex: 5,),
            //const SizedBox(height: 50.0),
            //const FlutterLogo(size: 100.0),
            const Expanded(child: FlutterLogo(size: 100.0),flex: 10,),
            const Spacer(flex: 1,),
            //const SizedBox(height: 100.0),
            const TextField(decoration: InputDecoration(labelText: 'Username')),
            const TextField(decoration: InputDecoration(labelText: 'Password')),
            const Spacer(flex: 10,),
            //const SizedBox(height: 30.0),
            ElevatedButton(onPressed: () {}, child: const Text('Login')),
          ],
        ),
      ),
    );
  }
}
